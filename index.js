'ust strict'

const requestAdr = 'https://studika.ru/api/areas'

function sendRequest(method, url) {
  return fetch(url, { method })
}

let country;
sendRequest('POST', requestAdr)
  .then(data => data.json())
  .then(data => { country = data })
  .catch(err => console.log(err))
const div = document.createElement('div')
div.className = 'modal'

const button = document.createElement('button')
button.className = 'button-region'
button.innerText = 'Сохранить'
const selectedLocation = document.createElement('div')
selectedLocation.className = 'selected-locations'
const allLocations = document.createElement('div')
allLocations.className = 'all-locations'
let addedLocations = []

function handleClick(e) {
  regions.after(div)
  div.innerHTML = '<input class = "input-region" list = "city" placeholder = "Регион, город, населенный пункт"></input>'
  div.append(selectedLocation)
  div.append(allLocations)
  div.append(button)
  addStr()
  e.stopPropagation()
}

const regions = document.querySelector(".city")
regions.addEventListener("click", handleClick)
div.addEventListener('click', (e) => e.stopPropagation())

let areaAndCities = []

function addStr() {
  function handleInput(event) {
    let corAreaCities = [...areaAndCities]
    let changetAreaCity = []
    let inputData = event.currentTarget.value
    let location = document.querySelectorAll('.location');
    if (inputData.length > 0) {
      for (let i = 0; i < location.length; i++) {
        location[i].parentNode.removeChild(location[i]);
      }
    }
    corAreaCities.forEach((item) => { item.toLowerCase().startsWith(inputData.toLowerCase()) ? changetAreaCity.push(item) : null })
    changetAreaCity.map((i) => {
      const location = document.createElement('div')
      location.className = 'location'
      location.innerText = `${(i)}`
      allLocations.append(location)
    })
    if (inputData.length == 0) {
      for (let i = 0; i < location.length; i++) {
        location[i].parentNode.removeChild(location[i]);
      }
    }

    let element = allLocations.querySelectorAll('.location')
    element.forEach((elem) => { elem.addEventListener('click', () => (addedLocations.includes(elem.innerText) == false) ? handleClickLocation(elem.innerText) : console.log('Регион уже добавлен')) }
    )

  }

  const inputRegion = document.querySelector('.input-region')
  inputRegion.addEventListener('input', handleInput,)

  country.forEach((item) => { areaAndCities.push(item.name); if (item.cities) { item.cities.forEach((item) => areaAndCities.push(item.name)) } })
  areaAndCities.map((i) => {
    const location = document.createElement('div')
    location.className = 'location'
    location.innerText = `${(i)}`
    allLocations.append(location)
  })

  function checkAddedLocations() {
    if (addedLocations.length > 0) { button.classList.add("push") }
    else { button.classList.remove("push") }
  }

  function handleClickLocation(event) {
    addedLocations.push(event)
    checkAddedLocations()
    const bobber = document.createElement('div')
    bobber.className = 'bobber'; bobber.innerText = `${event}`
    const bobberBtn = document.createElement('button')
    bobberBtn.className = 'bobber-btn'
    bobber.append(bobberBtn)
    selectedLocation.append(bobber)

    bobberBtn.addEventListener('click', () => (addedLocations.includes(bobber.innerText)) ?
      (addedLocations = addedLocations.filter((value) => { return value != bobber.innerText }), bobber.remove(), checkAddedLocations()) : null)
  }
  let element = allLocations.querySelectorAll('.location')
  element.forEach((elem) => { elem.addEventListener('click', () => (addedLocations.includes(elem.innerText) == false) ? handleClickLocation(elem.innerText) : console.log('Регион уже добавлен')) }
  )
}

button.addEventListener('click', function () { div.remove(); document.cookie = `regions = ${addedLocations}` })
window.addEventListener('click', () => div.remove())








